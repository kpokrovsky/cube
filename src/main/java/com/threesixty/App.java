package com.threesixty;

public class App {



    public static void main(String[] args ) {
        solveCubesForOneSolution();
        solveCubesForAllSolutions();
    }

    private static void solveCubesForOneSolution() {
        Solution blue = new Solution("./solution1_blue.txt");
        blue.findOneSolution(blue.BLUE_CUBE);

        Solution red = new Solution("./solution1_red.txt");
        red.findOneSolution(red.RED_CUBE);

        Solution purple = new Solution("./solution1_purple.txt");
        purple.findOneSolution(purple.PURPLE_CUBE);

        Solution yellow = new Solution("./solution1_yellow.txt");
        yellow.findOneSolution(yellow.YELLOW_CUBE);
    }

    private static void solveCubesForAllSolutions() {
        Solution blue = new Solution("./solution2_blue.txt");
        blue.findAllSolutions(blue.BLUE_CUBE);

        Solution red = new Solution("./solution2_red.txt");
        red.findAllSolutions(red.RED_CUBE);

        Solution purple = new Solution("./solution2_purple.txt");
        purple.findAllSolutions(purple.PURPLE_CUBE);

        Solution yellow = new Solution("./solution2_yellow.txt");
        yellow.findAllSolutions(yellow.YELLOW_CUBE);
    }

}
