package com.threesixty;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import static com.threesixty.Edge.EDGE_LENGTH;

/**
 * Print cube to stdtout/BufferedWriter
 */
public class CubePrinter {
    private static final char EMPTY = ' ';
    private static final char FILL = 'o';
    private static final int WIDTH = EDGE_LENGTH * 3;
    private static final int HEIGHT = EDGE_LENGTH * 4;

    private char[][] canvas = new char[HEIGHT][WIDTH];

    public CubePrinter() {
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WIDTH; j++) {
                canvas[i][j] = EMPTY;
            }
        }
    }

    public static void drawCube(List<Face> cube) {
        CubePrinter cp = new CubePrinter();
        cp.drawFaceOnCanvas(cube.get(0), 0, 0);
        cp.drawFaceOnCanvas(cube.get(1), 0, EDGE_LENGTH);
        cp.drawFaceOnCanvas(cube.get(2), 0, EDGE_LENGTH * 2);
        cp.drawFaceOnCanvas(cube.get(3), EDGE_LENGTH, EDGE_LENGTH);
        cp.drawFaceOnCanvas(cube.get(4), EDGE_LENGTH * 2, EDGE_LENGTH);
        cp.drawFaceOnCanvas(cube.get(5), EDGE_LENGTH * 3, EDGE_LENGTH);
        cp.sendCanvasToStdout();
    }

    public static void drawCube(List<Face> cube, BufferedWriter writer) {
        CubePrinter cp = new CubePrinter();
        cp.drawFaceOnCanvas(cube.get(0), 0, 0);
        cp.drawFaceOnCanvas(cube.get(1), 0, EDGE_LENGTH);
        cp.drawFaceOnCanvas(cube.get(2), 0, EDGE_LENGTH * 2);
        cp.drawFaceOnCanvas(cube.get(3), EDGE_LENGTH, EDGE_LENGTH);
        cp.drawFaceOnCanvas(cube.get(4), EDGE_LENGTH * 2, EDGE_LENGTH);
        cp.drawFaceOnCanvas(cube.get(5), EDGE_LENGTH * 3, EDGE_LENGTH);
        cp.sendCanvasToWriter(writer);
    }

    private void drawFaceOnCanvas(Face face, int offsetX, int offsetY) {
        drawFaceEdges(face, offsetX, offsetY);
        fillFace(offsetX, offsetY);
    }

    private void drawFaceEdges(Face face, int offsetX, int offsetY) {
        for (int i = 0, mask = 1 << (EDGE_LENGTH - 1); i < EDGE_LENGTH - 1; i++, mask >>>= 1) {
            if (face.getTopEdge().maskMatches(mask))
                canvas[offsetX][offsetY + i] = FILL;
            if (face.getRightEdge().maskMatches(mask))
                canvas[offsetX + i][offsetY + (EDGE_LENGTH - 1)] = FILL;
            if (face.getBottomEdge().maskMatches(mask))
                canvas[offsetX + (EDGE_LENGTH - 1)][offsetY + (EDGE_LENGTH - 1) - i] = FILL;
            if (face.getLeftEdge().maskMatches(mask))
                canvas[offsetX + (EDGE_LENGTH - 1) - i][offsetY] = FILL;
        }
    }

    private void fillFace(int offsetX, int offsetY) {
        for (int i = 0; i < (EDGE_LENGTH - 2); i++) {
            for (int j = 0; j < (EDGE_LENGTH - 2); j++) {
                canvas[offsetX + 1 + i][offsetY + 1 + j] = FILL;
            }
        }
    }

    private void sendCanvasToStdout() {
        for (int i = 0; i < HEIGHT; i++) {
            System.out.println(canvas[i]);
        }
    }

    private void sendCanvasToWriter(BufferedWriter writer) {
        try {
            for (int i = 0; i < HEIGHT; i++) {
                writer.write(canvas[i]);
                writer.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException("Can't write to file", e);
        }
    }
}
