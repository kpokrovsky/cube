package com.threesixty;

/**
 * The representation of edge
 */
class Edge {
    private int data = 0;
    static int EDGE_LENGTH = 5;

    Edge(int data) {
        this.data = data;
    }

    int getData() {
        return data;
    }

    int getFirstVertex() {
        return data & 1;
    }

    int getLastVertex() {
        return (data >>> (EDGE_LENGTH - 1)) & 1;
    }

    void flip() {
        int newData = 0;
        for (int i = 0; i < EDGE_LENGTH; i++) {
            newData ^= data & 1;
            newData <<= 1;
            data >>>= 1;
        }
        data = (newData >>> 1);
    }

    boolean match(Edge e) {
        if (areThereBothFirstVertices(this, e) || areThereBothLastVertices(this, e)) {
            return false;
        } else {
            return doesMidOfEdgesMatchCompletely(this, e);
        }

    }

    boolean maskMatches(int mask) {
        return (data & mask) == mask;
    }

    private boolean areThereBothFirstVertices(Edge e1, Edge e2) {
        return ((e1.data & 0b1) == 1) && ((e2.data >> (EDGE_LENGTH - 1)) == 1);
    }

    private boolean areThereBothLastVertices(Edge e1, Edge e2) {
        return areThereBothFirstVertices(e2, e1);
    }

    private boolean doesMidOfEdgesMatchCompletely(Edge e1, Edge e2) {
        for (int i = 0, leftMask = 0b01000, rightMask = 0b00010; i < EDGE_LENGTH - 2; i++, rightMask <<= 1, leftMask >>= 1) {
            if ((e1.data & rightMask) == (e2.data & leftMask)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return String.format("Edge{data=%05d}", Integer.valueOf(Long.toBinaryString(data)));
    }

    boolean dataEquals(Edge e) {
        return this.data == e.data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge edge = (Edge) o;

        return data == edge.data;
    }

    @Override
    public int hashCode() {
        return data;
    }
}
