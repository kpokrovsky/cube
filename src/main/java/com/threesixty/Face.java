package com.threesixty;

import java.util.LinkedList;

/**
 * The representation of cube's face
 */
class Face {
    static int NUMBER_OF_EDGES = 4;
    private LinkedList<Edge> edges = new LinkedList<>();
    private String id;

    Face(String id, int upEdge, int rightEdge, int bottomEdge, int leftEdge) {
        edges.addLast(new Edge(upEdge));
        edges.addLast(new Edge(rightEdge));
        edges.addLast(new Edge(bottomEdge));
        edges.addLast(new Edge(leftEdge));
        this.id = id;
    }

    String encodeFace() {
        StringBuilder sb = new StringBuilder();
        edges.forEach(edge -> sb.append(edge.getData()));
        return sb.toString();
    }

    Edge getTopEdge() {
        return edges.get(0);
    }

    Edge getRightEdge() {
        return edges.get(1);
    }

    Edge getBottomEdge() {
        return edges.get(2);
    }

    Edge getLeftEdge() {
        return edges.get(3);
    }

    void rotate() {
        edges.addLast(edges.removeFirst());
    }

    void flip() {
        edges.forEach(Edge::flip);
        Edge rightEdge = getRightEdge();
        Edge leftEdge = getLeftEdge();
        edges.set(1, leftEdge);
        edges.set(3, rightEdge);
    }

    boolean makesSenseToRotate() {
        return !(getTopEdge().dataEquals(getRightEdge()) && getTopEdge().dataEquals(getBottomEdge()) && getTopEdge().dataEquals(getLeftEdge()));
    }

    @Override
    public String toString() {
        return String.format("Face{id=%s, top=%s, right=%s, bottom=%s, left=%s}", id, getTopEdge(), getRightEdge(), getBottomEdge(), getLeftEdge());
    }
}
