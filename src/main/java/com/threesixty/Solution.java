package com.threesixty;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * The solution producer. State is not preserved upon execution.
 */
class Solution {

    private final Face BLUE_FACE_1 = new Face("blue-1", 0b00100, 0b00100, 0b00100, 0b00100);
    private final Face BLUE_FACE_2 = new Face("blue-2", 0b10101, 0b11011, 0b10101, 0b11011);
    private final Face BLUE_FACE_3 = new Face("blue-3", 0b00100, 0b01010, 0b00100, 0b00100);
    private final Face BLUE_FACE_4 = new Face("blue-4", 0b01010, 0b00100, 0b01011, 0b11010);
    private final Face BLUE_FACE_5 = new Face("blue-5", 0b01010, 0b01010, 0b00101, 0b11010);
    private final Face BLUE_FACE_6 = new Face("blue-6", 0b01010, 0b01011, 0b11011, 0b10100);

    private final Face RED_FACE_1 = new Face("red-1", 0b00011, 0b10101, 0b11010, 0b00100);
    private final Face RED_FACE_2 = new Face("red-2", 0b01010, 0b00100, 0b00010, 0b01010);
    private final Face RED_FACE_3 = new Face("red-3", 0b01101, 0b11011, 0b11001, 0b11010);
    private final Face RED_FACE_4 = new Face("red-4", 0b00100, 0b00100, 0b00100, 0b01010);
    private final Face RED_FACE_5 = new Face("red-5", 0b00110, 0b01010, 0b00101, 0b11010);
    private final Face RED_FACE_6 = new Face("red-6", 0b01100, 0b00101, 0b11011, 0b10100);

    private final Face PURPLE_FACE_1 = new Face("purple-1", 0b11010, 0b00010, 0b00100, 0b00111);
    private final Face PURPLE_FACE_2 = new Face("purple-2", 0b00011, 0b10100, 0b01010, 0b00110);
    private final Face PURPLE_FACE_3 = new Face("purple-3", 0b01000, 0b00100, 0b00100, 0b01010);
    private final Face PURPLE_FACE_4 = new Face("purple-4", 0b11011, 0b11000, 0b01010, 0b00101);
    private final Face PURPLE_FACE_5 = new Face("purple-5", 0b00101, 0b11100, 0b01101, 0b11100);
    private final Face PURPLE_FACE_6 = new Face("purple-6", 0b01011, 0b10100, 0b01011, 0b11000);

    private final Face YELLOW_FACE_1 = new Face("yellow-1", 0b00100, 0b00100, 0b01010, 0b01010);
    private final Face YELLOW_FACE_2 = new Face("yellow-2", 0b00101, 0b11000, 0b01010, 0b01010);
    private final Face YELLOW_FACE_3 = new Face("yellow-3", 0b00101, 0b11010, 0b00101, 0b11100);
    private final Face YELLOW_FACE_4 = new Face("yellow-4", 0b10101, 0b11010, 0b00101, 0b11011);
    private final Face YELLOW_FACE_5 = new Face("yellow-5", 0b00100, 0b01010, 0b01011, 0b10100);
    private final Face YELLOW_FACE_6 = new Face("yellow-6", 0b01010, 0b00101, 0b11010, 0b00100);

    final Face[] BLUE_CUBE = new Face[] { BLUE_FACE_1, BLUE_FACE_2, BLUE_FACE_3, BLUE_FACE_4, BLUE_FACE_5, BLUE_FACE_6 };
    final Face[] RED_CUBE = new Face[] { RED_FACE_1, RED_FACE_2, RED_FACE_3, RED_FACE_4, RED_FACE_5, RED_FACE_6 };
    final Face[] PURPLE_CUBE = new Face[] { PURPLE_FACE_1, PURPLE_FACE_2, PURPLE_FACE_3, PURPLE_FACE_4, PURPLE_FACE_5, PURPLE_FACE_6};
    final Face[] YELLOW_CUBE = new Face[] { YELLOW_FACE_1, YELLOW_FACE_2, YELLOW_FACE_3, YELLOW_FACE_4, YELLOW_FACE_5, YELLOW_FACE_6};

    private LinkedList<Face> availableFaces;
    private LinkedList<Face> solution = new LinkedList<>();
    private HashSet<String> encounteredSolutions = new HashSet<>();

    private Face[] cube;
    private boolean solutionHasBeenFound = false;
    private Supplier<Boolean> breakRecursion = () -> false;
    private BufferedWriter fileWriter;

    Solution(String filename) {
        try {
            this.fileWriter = Files.newBufferedWriter(Paths.get(filename), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException("Can't open the file", e);
        }
    }

    void findOneSolution(Face[] cube) {
        this.cube = cube;
        breakRecursion = () -> solutionHasBeenFound;
        solve();
        closeOutputFile();
    }

    void findAllSolutions(Face[] cube) {
        this.cube = cube;
        solve();
        closeOutputFile();
    }

    private void solve() {
        Permutations.of(cube)
                // filter out rotated cubes
                .filter(this::onlyCubesThatStartFromFirstFace)
                .filter(permutation -> onlyCubesThatHaveSecondFaceInSecondPosition(permutation) ||
                        onlyCubesThatHaveSecondFaceInThirdPositionAndThirdFaceInSecondPosition(permutation))
                .forEachOrdered(this::rotateAndFlipFacesForSolution);
    }

    private boolean onlyCubesThatStartFromFirstFace(List<Face> permutation) {
        return Objects.equals(permutation.get(0), cube[0]);
    }

    private boolean onlyCubesThatHaveSecondFaceInSecondPosition(List<Face> permutation) {
        return Objects.equals(permutation.get(1), cube[1]);
    }

    private boolean onlyCubesThatHaveSecondFaceInThirdPositionAndThirdFaceInSecondPosition(List<Face> permutation) {
        return Objects.equals(permutation.get(1), cube[2]) && Objects.equals(permutation.get(2), cube[1]);
    }

    private void rotateAndFlipFacesForSolution(List<Face> cube) {
        availableFaces = new LinkedList<>(cube);
        recursion();
    }

    private void recursion() {
        if (breakRecursion.get()) {
            return;
        }

        if (availableFaces.isEmpty()) {
            if (isValidCube(solution) && !encounteredSolutions.contains(encodeCube(solution))) {
                encounteredSolutions.add(encodeCube(solution));
                solutionHasBeenFound = true;
                CubePrinter.drawCube(solution, fileWriter);
            }
            return;
        }

        Face nextFace = availableFaces.removeFirst();
        solution.addLast(nextFace);
        if (nextFace.makesSenseToRotate()) {
            for (int i = 0; i < Face.NUMBER_OF_EDGES; i++) {
                recursion();
                nextFace.rotate();
            }
            if (isItNotFirstFace()) { // filter out mirrored solutions: never flip first face
                nextFace.flip();
                for (int i = 0; i < Face.NUMBER_OF_EDGES; i++) {
                    recursion();
                    nextFace.rotate();
                }
                nextFace.flip();
            }
        } else {
            recursion();
        }

        availableFaces.addFirst(solution.removeLast());
    }

    private boolean isItNotFirstFace() {
        return availableFaces.size() != 5;
    }

    private String encodeCube(List<Face> cube) {
        StringBuilder sb = new StringBuilder();
        cube.forEach(face -> sb.append(face.encodeFace()));
        return sb.toString();
    }

    private boolean isValidCube(List<Face> cube) {
        assert cube.size() == 6;
        return validateEdges(cube) && validateVertices(cube);
    }

    private boolean validateEdges(List<Face> cube) {
        Face firstFace = cube.get(0);
        Face secondFace = cube.get(1);
        Face thirdFace = cube.get(2);
        Face fourthFace = cube.get(3);
        Face fifthFace = cube.get(4);
        Face sixthFace = cube.get(5);

        return
                firstFace.getRightEdge().match(secondFace.getLeftEdge()) &&
                        firstFace.getBottomEdge().match(fourthFace.getLeftEdge()) &&
                        firstFace.getLeftEdge().match(fifthFace.getLeftEdge()) &&
                        firstFace.getTopEdge().match(sixthFace.getLeftEdge()) &&
                        thirdFace.getLeftEdge().match(secondFace.getRightEdge()) &&
                        thirdFace.getBottomEdge().match(fourthFace.getRightEdge()) &&
                        thirdFace.getRightEdge().match(fifthFace.getRightEdge()) &&
                        thirdFace.getTopEdge().match(sixthFace.getRightEdge()) &&
                        secondFace.getBottomEdge().match(fourthFace.getTopEdge()) &&
                        fourthFace.getBottomEdge().match(fifthFace.getTopEdge()) &&
                        fifthFace.getBottomEdge().match(sixthFace.getTopEdge()) &&
                        sixthFace.getBottomEdge().match(secondFace.getTopEdge());
    }

    private boolean validateVertices(List<Face> cube) {
        Face firstFace = cube.get(0);
        Face secondFace = cube.get(1);
        Face thirdFace = cube.get(2);
        Face fourthFace = cube.get(3);
        Face fifthFace = cube.get(4);
        Face sixthFace = cube.get(5);

        return (firstFace.getRightEdge().getLastVertex() + secondFace.getLeftEdge().getFirstVertex() + sixthFace.getBottomEdge().getFirstVertex() == 1) &&
                (firstFace.getRightEdge().getFirstVertex() + secondFace.getLeftEdge().getLastVertex() + fourthFace.getTopEdge().getLastVertex() == 1) &&
                (secondFace.getRightEdge().getLastVertex() + thirdFace.getLeftEdge().getFirstVertex() + sixthFace.getBottomEdge().getLastVertex() == 1) &&
                (secondFace.getRightEdge().getFirstVertex() + thirdFace.getLeftEdge().getLastVertex() + fourthFace.getTopEdge().getFirstVertex() == 1) &&
                (firstFace.getLeftEdge().getLastVertex() + fourthFace.getLeftEdge().getLastVertex() + fifthFace.getLeftEdge().getFirstVertex() == 1) &&
                (firstFace.getLeftEdge().getFirstVertex() + fifthFace.getLeftEdge().getLastVertex() + sixthFace.getLeftEdge().getFirstVertex() == 1) &&
                (thirdFace.getRightEdge().getFirstVertex() + fourthFace.getRightEdge().getFirstVertex() + fifthFace.getRightEdge().getLastVertex() == 1) &&
                (thirdFace.getRightEdge().getLastVertex() + fifthFace.getRightEdge().getFirstVertex() + sixthFace.getRightEdge().getLastVertex() == 1);
    }

    private void closeOutputFile() {
        try {
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException("Can't close the file", e);
        }
    }

}
