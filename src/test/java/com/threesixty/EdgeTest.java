package com.threesixty;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EdgeTest {

    @Test
    public void fullMatchEdgesTest() {
        Edge firstValidEdge = new Edge(0b01110);
        Edge secondValidEdge = new Edge(0b10001);
        Edge thirdValidEdge = new Edge(0b10111);
        Edge fourthValidEdge = new Edge(0b00010);
        Edge invalidEdge = new Edge(0b01111);
        assertTrue(firstValidEdge.match(secondValidEdge));
        assertTrue(secondValidEdge.match(firstValidEdge));
        assertFalse(firstValidEdge.match(invalidEdge));
        assertFalse(invalidEdge.match(firstValidEdge));
        assertTrue(thirdValidEdge.match(fourthValidEdge));
        assertTrue(fourthValidEdge.match(thirdValidEdge));
    }

    @Test
    public void bothVerticesTest() {
        Edge firstVertex1 = new Edge(0b01111);
        Edge firstVertex2 = new Edge(0b10000);
        assertFalse(firstVertex1.match(firstVertex2));

        Edge lastVertex1 = new Edge(0b11110);
        Edge lastVertex2 = new Edge(0b00001);
        assertFalse(lastVertex1.match(lastVertex2));

        Edge lastVertex3 = new Edge(0b10100);
        Edge lastVertex4 = new Edge(0b11011);
        assertFalse(lastVertex3.match(lastVertex4));
        assertFalse(lastVertex4.match(lastVertex3));

        Edge bothVertices1 = new Edge(0b11111);
        Edge bothVertices2 = new Edge(0b10001);
        assertFalse(bothVertices1.match(bothVertices2));
    }

    @Test
    public void validPartialMatchEdgesTest() {
        Edge firstEdge = new Edge(0b01110);
        Edge secondEdge = new Edge(0b00000);
        assertTrue(firstEdge.match(secondEdge));
        assertTrue(secondEdge.match(firstEdge));
    }

    @Test
    public void invalidPartialMatchEdgesTest() {
        Edge firstEdge = new Edge(0b01010);
        Edge secondEdge = new Edge(0b00000);
        assertFalse(firstEdge.match(secondEdge));
        assertFalse(secondEdge.match(firstEdge));
    }

    @Test
    public void flipTest() {
        Edge edge = new Edge(0b01001);
        Edge flippedEdge = new Edge(0b10010);
        edge.flip();
        assertEquals(flippedEdge, edge);
    }
}
